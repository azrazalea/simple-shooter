;;;; simple-shooter-test.asd

(asdf:defsystem #:simple-shooter-test
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :depends-on (:simple-shooter
               :prove
               :check-it)
  :pathname "src/test"
  :components ((:file "suite"))
  :description "Testing Simple Shooter")
