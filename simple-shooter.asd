;;;; simple-shooter.asd

(asdf:defsystem #:simple-shooter
  :description "A simple shooting game."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :depends-on ("sketch"
               "split-sequence"
               "alexandria"
               "iterate")
  :serial t
  :pathname "src"
  :components ((:file "package")
               (:file "player")
               (:file "graphics")
               (:file "internal")))
