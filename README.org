* Simple Shooter
** Description
A simple shooting game. As of this time I'm not sure what form it will take (similar to asteroids, space invaders, bullet hell?)
** Install
- Install [[https://www.quicklisp.org/beta/][quicklisp]]
- Clone into ~/quicklisp/local-projects/
- Clone this git repo into ~/quicklisp/local-projects/
- (ql:quickload :simple-shooter)
- (simple-shooter:start)
** License
Copyright (C) 2016 Lily Carpenter

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
