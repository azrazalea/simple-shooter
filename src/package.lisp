;;;; package.lisp

(defpackage #:simple-shooter
  (:use)
  (:export #:start))

(defpackage #:simple-shooter-player
  (:use :cl)
  (:export #:player
           #:make-player
           #:x-pos
           #:y-pos))

(defpackage #:simple-shooter-graphics
  (:use #:cl
        #:simple-shooter-player
        #:sketch)
  (:export #:simple-shooter-sketch))

(defpackage #:simple-shooter-internal
  (:use #:cl
        #:split-sequence
        #:simple-shooter-player
        #:simple-shooter-graphics
        #:simple-shooter))
