;;;; Copyright (C) 2016  Lily Carpenter

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;; graphics.lisp

(in-package :simple-shooter-graphics)

(defparameter *player-pen* (make-pen :stroke (gray 0) :fill (rgb-255 200 200 200)))
(defparameter *player* (make-player))

(defun draw-player (player)
  (with-slots (x-pos y-pos) player
    (with-pen
        (ngon 3 x-pos y-pos 15 15 270))))

(defsketch simple-shooter-sketch
    (:title "Simple Shooter"
     :width 800
     :height 600
     :debug :scancode-f1)
    ()
    (background +black+)
    (draw-player *player*))

(defmethod kit.sdl2:keyboard-event ((window simple-shooter-sketch)
                                    state
                                    timestamp
                                    repeat-p
                                    keysym)
  (with-slots (x-pos y-pos speed) *player*
    (case (sdl2:scancode keysym)
      (:scancode-left (decf x-pos speed))
      (:scancode-right (incf x-pos speed))
      (:scancode-up (decf y-pos speed))
      (:scancode-down (incf y-pos speed)))))
